=begin
doctest: I have a method called hello
>> defined?(hello)
=> 'method'
doctest: hello returns "Hello World!"
>> hello
=> "Hello World!"
doctest: I can greet by name
>> hello("Tarun")
=> "Hello Tarun!"
doctest: I can ask a question
>> hello("Victor", "?")
=> "Hello Victor?"
=end

def hello(name='World', punctuation = '!')
  "Hello #{name}#{punctuation}"
end
