def leap_year?(year)
  year % 400 == 0 || year % 100 == 0 && year % 4   == 0 
end

def minutes_in_year(year)
  if leap_year?(year)
    366*24*60
  else
    365*24*60
  end
end

def is_or_not(boolean)
  boolean ? 'is' : 'is not'
end
  
if __FILE__ == $0
  [20004, 2000, 1996, 1900, 1800, 1802].each do |year|
    puts "Year #{year} #{is_or_not(leap_year?(year))} a leap year. There are #{minutes_in_year(year)} minutes in a leap year."
  end
end

=begin
doctest: I have a method named leap_year?
>> defined?(leap_year?)  
=> 'method'
doctest: leap_year? given a year that is a leap year returns true
>> leap_year?(2000) {|min, year| }
=> true
>> leap_year?(1996)
=> true
doctest: leap_year? given a year that is not a leap year returns false
>> leap_year?(1999)
=> false
doctest: leap_year? given 1900 returns false
>> leap_year?(1900)
=> false
=end

